--[[
Copyright (c) 2023 Felix von Oertzen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.]]

function conky_format( format, number )
    return string.format( format, conky_parse( number ) )
end


require 'cairo'

WHITE = 0xffffff
RED = 0xbb2200

function conky_main()
  if conky_window == nil then
      return
  end
  local cs = cairo_xlib_surface_create (conky_window.display,
                                       conky_window.drawable,
                                       conky_window.visual,
                                       conky_window.width,
                                       conky_window.height)
  cr = cairo_create (cs)

  cpu_temp = tonumber(conky_parse("${exec awk '{print int($1/1000)}' < /sys/class/hwmon/hwmon2/temp2_input}"))
  cpu_usag  = tonumber(conky_parse("$cpu"))
  cpu_freq = tonumber(conky_parse("$freq"))
 
  gauges = {
    {
      x = 635,
      y = 285,
      radius = 200,
      value = cpu_freq,
      danger_value = 4000,
      max_value = 4580,
      unit = 'MHz',
      label = ''
    },
    {
      x = 250,
      y = 319,
      radius = 150,
      value = cpu_usag,
      danger_value = 85,
      max_value = 100,
      unit = '%',
      label = ''
    },
    {
      x = 1030,
      y = 319,
      radius = 150,
      value = cpu_temp,
      danger_value = 85,
      max_value = 100,
      unit = '°C',
      label = ''
    }
  }
  for _,g in ipairs(gauges) do
    gauge(cr, g)
  end
  cairo_destroy (cr)
  cairo_surface_destroy (cs)
  cr = nil
end

function colour(c, a)
	return ((c / 0x10000) % 0x100) / 255, ((c / 0x100) % 0x100) / 255, (c % 0x100) / 255, a
end

function deg_to_rad(d)
  return (d - 90) * (math.pi / 180)
end

function clamp_deg(d)
  while d>360 do d=d-360 end 
  while d<0 do d=d+360 end
  return d
end

function polar( cx, cy, deg, radius )
  rad = deg_to_rad(deg)
  x = (radius * math.cos(rad)) + cx
  y = (radius * math.sin(rad)) + cy
  return x, y
end

function arc( cr, start_angle, end_angle, x, y, radius )
  cairo_arc (cr, x, y, radius, deg_to_rad(start_angle), deg_to_rad(end_angle))
end

function gauge( cr, g)
  radius = g.radius 
  start_angle = 250
  end_angle = 110
  range = end_angle - start_angle + 360
  danger_angle = clamp_deg(start_angle + ((g.danger_value / g.max_value ) * range))
  value_angle = start_angle + ((g.value / g.max_value ) * range)

  -- background
  arc(cr, start_angle*0.95, 1.1*end_angle+360,g.x,g.y, radius*1.15) -- dynamisch machen!
  cairo_set_source_rgb(cr, 0.2, 0.2, 0.2); 
  cairo_fill(cr);
  
  -- "speed" markers
  cairo_stroke (cr)
  cairo_set_line_width (cr, 3)
  cairo_set_source_rgba (cr, colour(WHITE,1))
  cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND)
  for i = 0,100, 5
  do
    angle = start_angle + i/100 * range
    if i >= 100*((g.danger_value / g.max_value )) then
      cairo_set_source_rgba (cr, colour(RED,1))
    else
      cairo_set_source_rgba (cr, colour(WHITE,1))
    end
    cairo_move_to( cr, polar( g.x, g.y, angle, radius * 15/16 ))
    cairo_line_to( cr, polar( g.x, g.y, angle, radius * 17/16 ))
    cairo_stroke( cr )
    if i % 10 == 0 then
      x,y = polar( g.x, g.y, angle, radius * 12/16 )
      text(cr, x - 12*radius/100,y, tostring(math.floor(g.max_value/100*i)) .. g.unit, 14, WHITE,1)
    end
  end
  -- der Zeiger
  cairo_set_line_width (cr, 4)
  cairo_set_source_rgba (cr, colour(RED,1))
  cairo_move_to( cr, polar( g.x, g.y, value_angle, radius * -1/8 ))
  cairo_line_to( cr, polar( g.x, g.y, value_angle, radius * 9/8 ))
  cairo_stroke( cr )
  
  -- der Mittelpunkt
  cairo_set_source_rgba (cr, colour(WHITE,1))
  cairo_move_to( cr, polar( g.x, g.y, 0, 0 ))
  cairo_set_line_width(cr, 10);  
  arc(cr, 0,0, g.x, g.y, 0)
  cairo_stroke (cr)
  
  text(cr, g.x+20, g.y+radius*1/4, g.value .. g.unit, 12, LIGHTBLUE, 1 )
  text(cr, g.x+20+radius, g.y, g.label, 16, WHITE, 1 )
end

function text( cr, x, y, text, size, text_color, text_alpha)
  font = "DejaVu Sans Mono"
  font_slant = CAIRO_FONT_SLANT_NORMAL
  font_face = CAIRO_FONT_WEIGHT_NORMAL

  cairo_select_font_face (cr, font, font_slant, font_face);
  cairo_set_font_size (cr, size)
  cairo_set_source_rgba (cr, colour(text_color,text_alpha))
  cairo_move_to (cr, x, y)
  cairo_show_text (cr, text)
  cairo_stroke (cr)
end
